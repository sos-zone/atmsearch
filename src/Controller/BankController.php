<?php

namespace App\Controller;

use App\Service\PrivatBankProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BankController extends AbstractController
{
    /**
     * @param Request $request
     * @param PrivatBankProvider $bankProvider
     *
     * @return Response
     *
     * @Route("/", name="get_bank_atms")
     */
    public function index(
        Request $request,
        PrivatBankProvider $bankProvider
    ): Response {
        $city = $request->query->get('city');
        $address = $request->query->get('address');

        $atms = $bankProvider->getATMListByCityAndAddress($city, $address);

        if (!$atms) {
            return new Response('Invalid Request', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->render('default/index.html.twig', [
            'city' => $city,
            'address' => $address,
            'atms' => $atms,
        ]);
    }
}