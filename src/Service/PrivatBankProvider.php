<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;

class PrivatBankProvider
{
    private const API_POINT = 'https://api.privatbank.ua/p24api/infrastructure?json';

    /**
     * @var \Symfony\Contracts\HttpClient\HttpClientInterface
     */
    private $client;

    public function __construct()
    {
        $this->client = HttpClient::create();
    }

    /**
     * @param string|null $city
     * @param string|null $address
     *
     * @return array|null
     */
    public function getATMListByCityAndAddress(string $city = null, string $address = null): ?array
    {
        $url = $this->getUrlByCityAndAddress($city, $address);

        $content = $this->doRequest($url);

        if (!$content) {
            return null;
        }

        return \json_decode($content, true);
    }

    private function getUrlByCityAndAddress(?string $city, ?string $address)
    {
        $url = self::API_POINT;

        $url .= '&atm';

        if ($city) {
            $url .= "&city=$city";
        }

        if ($address) {
            $url .= "&address=$address";
        }

        return $url;
    }

    /**
     * @param string $url
     *
     * @return string|null
     */
    private function doRequest(string $url): ?string
    {
        try {
            $response = $this->client->request('GET', $url);

            $content = $response->getContent();
        }
        catch (\Exception $e) {
            # log exception..
            return null;
        }

        return $content;
    }
}